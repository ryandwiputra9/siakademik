-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for SIAkademik
CREATE DATABASE IF NOT EXISTS `SIAkademik` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `SIAkademik`;

-- Dumping structure for table SIAkademik.ACTIVITY
CREATE TABLE IF NOT EXISTS `ACTIVITY` (
  `ACTIVITY_TYPE` varchar(50) DEFAULT NULL,
  `ACITIVITY_NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.ACTIVITY: ~0 rows (approximately)
/*!40000 ALTER TABLE `ACTIVITY` DISABLE KEYS */;
/*!40000 ALTER TABLE `ACTIVITY` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.GURU
CREATE TABLE IF NOT EXISTS `GURU` (
  `ID_GURU` varchar(50) DEFAULT NULL,
  `NIP` varchar(50) DEFAULT NULL,
  `NAMA` varchar(120) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(50) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `LULUSAN` varchar(50) DEFAULT NULL,
  `TELEPHONE` text,
  `EMAIL` text,
  `JABATAN` varchar(50) DEFAULT NULL,
  `STATUS_GURU` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.GURU: ~1 rows (approximately)
/*!40000 ALTER TABLE `GURU` DISABLE KEYS */;
INSERT INTO `GURU` (`ID_GURU`, `NIP`, `NAMA`, `TEMPAT_LAHIR`, `TANGGAL_LAHIR`, `AGAMA`, `LULUSAN`, `TELEPHONE`, `EMAIL`, `JABATAN`, `STATUS_GURU`) VALUES
	('1', '201993', 'Ryan D Putra', 'Bekasi', '1994-02-21', 'ISLAM', 'S1', '08822444', 'ryan@gmail.com', 'GURU', 1);
/*!40000 ALTER TABLE `GURU` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.ROLE
CREATE TABLE IF NOT EXISTS `ROLE` (
  `ROLE_ID` int(11) DEFAULT NULL,
  `ROLE` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.ROLE: ~4 rows (approximately)
/*!40000 ALTER TABLE `ROLE` DISABLE KEYS */;
INSERT INTO `ROLE` (`ROLE_ID`, `ROLE`) VALUES
	(0, 'SUPER_ADMIN'),
	(1, 'ADMIN'),
	(2, 'GURU'),
	(3, 'SISWA');
/*!40000 ALTER TABLE `ROLE` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.SISWA
CREATE TABLE IF NOT EXISTS `SISWA` (
  `ID` varchar(50) DEFAULT NULL,
  `NIS` int(11) DEFAULT NULL,
  `NISN` int(11) DEFAULT NULL,
  `NAMA` varchar(50) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(50) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TELEPHONE` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.SISWA: ~4 rows (approximately)
/*!40000 ALTER TABLE `SISWA` DISABLE KEYS */;
INSERT INTO `SISWA` (`ID`, `NIS`, `NISN`, `NAMA`, `TEMPAT_LAHIR`, `TANGGAL_LAHIR`, `AGAMA`, `TELEPHONE`) VALUES
	('1', 9988881, 123331, 'Linux mint 19 Sylvia', 'Afrika', '1990-10-05', 'ISLAM', '12345'),
	('2', 1224, 1235, 'Nikumi', 'Jakarta', '2000-02-18', 'ISLAM', '098887'),
	('3', 9991, 123331, 'Linux23', 'Jakarta', '1994-10-04', 'ISLAM', '0888877712'),
	('5', 9992, 123331, 'Mint', 'Jakarta', '1994-10-05', 'ISLAM', '0888877712');
/*!40000 ALTER TABLE `SISWA` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.USERS
CREATE TABLE IF NOT EXISTS `USERS` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(100) DEFAULT NULL,
  `FULL_NAME` text,
  `ACTIVE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `Index 1` (`USER_ID`,`EMAIL`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.USERS: ~4 rows (approximately)
/*!40000 ALTER TABLE `USERS` DISABLE KEYS */;
INSERT INTO `USERS` (`USER_ID`, `USERNAME`, `PASSWORD`, `EMAIL`, `FULL_NAME`, `ACTIVE`) VALUES
	(1, 'super', 'super', 'super@gmail.com', 'SUPER ADMIN', '1'),
	(2, 'admin', 'admin', 'admin@gmail.com', 'ADMIN', '1'),
	(3, 'guru', 'guru', 'guru@gmail.com', 'GURU', '1'),
	(4, 'siswa', 'siswa', 'siswa@gmail.com', 'SISWA', '1');
/*!40000 ALTER TABLE `USERS` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.USER_ACTIVITY
CREATE TABLE IF NOT EXISTS `USER_ACTIVITY` (
  `ID` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) DEFAULT NULL,
  `ACTIVITY_TYPE` varchar(50) DEFAULT NULL,
  `DATE_ACTIVITY` datetime DEFAULT NULL,
  `MODULE_NAME` varchar(100) DEFAULT NULL,
  `MODULE_CODE` varchar(50) DEFAULT NULL,
  `IP_ADDRESS` varchar(100) DEFAULT NULL,
  `USER_AGENT` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.USER_ACTIVITY: ~6 rows (approximately)
/*!40000 ALTER TABLE `USER_ACTIVITY` DISABLE KEYS */;
INSERT INTO `USER_ACTIVITY` (`ID`, `USER_ID`, `ACTIVITY_TYPE`, `DATE_ACTIVITY`, `MODULE_NAME`, `MODULE_CODE`, `IP_ADDRESS`, `USER_AGENT`) VALUES
	(00000000001, 1, NULL, '2019-03-05 17:22:19', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'),
	(00000000002, 1, NULL, '2019-03-05 17:23:24', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'),
	(00000000003, 1, NULL, '2019-03-05 17:24:09', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'),
	(00000000004, 1, NULL, '2019-03-05 17:24:44', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'),
	(00000000005, 1, NULL, '2019-03-05 17:24:49', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0'),
	(00000000006, 1, NULL, '2019-03-05 17:31:22', 'restActivity', 'getAll', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0');
/*!40000 ALTER TABLE `USER_ACTIVITY` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.USER_ROLE
CREATE TABLE IF NOT EXISTS `USER_ROLE` (
  `USER_ID` int(11) DEFAULT NULL,
  `ROLE_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.USER_ROLE: ~4 rows (approximately)
/*!40000 ALTER TABLE `USER_ROLE` DISABLE KEYS */;
INSERT INTO `USER_ROLE` (`USER_ID`, `ROLE_ID`) VALUES
	(1, 0),
	(2, 1),
	(3, 2),
	(4, 3);
/*!40000 ALTER TABLE `USER_ROLE` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.WALI_KELAS
CREATE TABLE IF NOT EXISTS `WALI_KELAS` (
  `ID_WALIKELAS` varchar(50) DEFAULT NULL,
  `ID_GURU` varchar(50) DEFAULT NULL,
  `KELAS` varchar(50) DEFAULT NULL,
  `TAHUN_AJARAN` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.WALI_KELAS: ~1 rows (approximately)
/*!40000 ALTER TABLE `WALI_KELAS` DISABLE KEYS */;
INSERT INTO `WALI_KELAS` (`ID_WALIKELAS`, `ID_GURU`, `KELAS`, `TAHUN_AJARAN`) VALUES
	('1', '1', '92', '2019-2020');
/*!40000 ALTER TABLE `WALI_KELAS` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
