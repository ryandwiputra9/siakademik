package com.sia;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import org.mockito.internal.util.collections.ArrayUtils;

public class jsonBinding {

	public static void main(String[] args) {
		Person person = new Person(1, "Jhon", "jhon@test.com", 20, LocalDate.of(2019, 9, 7), BigDecimal.valueOf(1000));
		Person person3 = new Person(2, "Jhon", "jhon@test.com", 20, LocalDate.of(2019, 9, 7), BigDecimal.valueOf(1000));
		Jsonb jsonb = JsonbBuilder.create();
		String jsonPerson = jsonb.toJson(person);
		String bookJson = JsonbBuilder.create().toJson(person);

		System.out.println(bookJson);

		Person person2 = jsonb.fromJson(jsonPerson, Person.class);
		System.out.println("get Data from JSON : " + person2.toString());

		/* List for Json */

		System.out.println("List For JSON ");
		List<Person> personList = Arrays.asList(person, person3);
		String jsonArrayPerson = jsonb.toJson(personList);
		System.out.println(jsonArrayPerson);

		// convert from JSON array to List we’ll use the fromJson API:
		System.out.println("Json Array to List ");

		List<Person> personList2 = jsonb.fromJson(jsonArrayPerson, new ArrayList<Person>() {
		}.getClass().getGenericSuperclass());
		
		
		
		// view Data from List without Loop
		// ---- harus di override toString di Objecy ----
		System.out.println("view Data from List without Loop");
		System.out.println(Arrays.toString(personList2.toArray()));
		System.out.println("view Data from List without Loop - 2");
		
		//Since Java 8
		personList2.forEach(System.out::println);
		
		System.out.println("view Data from List without Loop - 3");
		System.out.println(personList2);
		
		System.out.println("view Data from List without Loop - 4");
		personList2.stream().forEach(System.out::println);
		
		
		
	}
}
