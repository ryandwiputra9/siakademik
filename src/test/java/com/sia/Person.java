package com.sia;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.json.bind.annotation.JsonbNumberFormat;
import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;

public class Person {

	private int id;

	@JsonbProperty("person-name")
	private String name;

	@JsonbProperty(nillable = true)
	private String email;

	@JsonbTransient
	private int age;

	@JsonbDateFormat("dd-MM-yyyy")
	private LocalDate registeredDate;

	private BigDecimal salary;

	@JsonbNumberFormat(locale = "en_US", value = "#0.0")
	public BigDecimal getSalary() {
		return salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public LocalDate getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(LocalDate registeredDate) {
		this.registeredDate = registeredDate;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Person {id:" + id + ", name:" + name + ", email:" + email + ", age:" + age + ", registeredDate:"
				+ registeredDate + ", salary:" + salary + "}";
	}

	public Person(int id, String name, String email, int age, LocalDate registeredDate, BigDecimal salary) {
	//	super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.age = age;
		this.registeredDate = registeredDate;
		this.salary = salary;
	}

	public Person() {

	}

}
