/*package com.sia;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.patimura.common.exception.ValidationException;
import org.patimura.common.parameter.ParameterHelper;
import org.patimura.common.parameter.PropertiesManager;
import org.patimura.util.datatype.ApplicationMessage;
import org.patimura.util.datatype.Information;
import org.patimura.util.datatype.MessagesContainer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.infinetworks.ib.cache.IbCachedObjectManager;
import com.infinetworks.ib.dao.ExecutionDao;
import com.infinetworks.ib.dao.MassTrfDao;
import com.infinetworks.ib.dao.MassTrfErrorDetailDao;
import com.infinetworks.ib.dao.TaskDao;
import com.infinetworks.ib.dao.TaskTypeDao;
import com.infinetworks.ib.dao.TransferDao;
import com.infinetworks.ib.dao.TransferDomDao;
import com.infinetworks.ib.entities.AccountInformation;
import com.infinetworks.ib.entities.BaseFee;
import com.infinetworks.ib.entities.BaseTransfer;
import com.infinetworks.ib.entities.Currency;
import com.infinetworks.ib.entities.ExchangeRates;
import com.infinetworks.ib.entities.MassTrfDetail;
import com.infinetworks.ib.entities.MassTrfErrorDetail;
import com.infinetworks.ib.entities.TaskType;
import com.infinetworks.ib.entities.Transaction;
import com.infinetworks.ib.entities.Transfer;
import com.infinetworks.ib.entities.TransfersDom;
import com.infinetworks.ib.entities.User;
import com.infinetworks.ib.sao.MassTransferSao;
import com.infinetworks.ib.sao.TransferSao;
import com.infinetworks.ib.services.TaskMassTransferService;
import com.infinetworks.ib.services.TaskService;
import com.infinetworks.ib.services.TaskTransferDomesticService;
import com.infinetworks.ib.services.TaskTransferRealtimeService;
import com.infinetworks.ib.services.TaskTransferService;
import com.infinetworks.ib.utils.GeneralHelper;
import com.infinetworks.ib.utils.MessageCodes;
import com.infinetworks.ib.utils.QNBGeneralHelper;

@Component
public class MassTransferSaoImpl extends AbstractSao implements MassTransferSao, InitializingBean {
	
	@Autowired
	@Qualifier("qnbTransferWithinBankService")
	private TaskTransferService transferService;
	
	@Autowired
	@Qualifier("qnbTransferDomesticService")
	private TaskTransferDomesticService domesticService;
	
	@Autowired
	@Qualifier("qnbTransferRealtimeService")
	private TaskTransferRealtimeService realtimeService;
	
	@Autowired
	private TaskMassTransferService massTransferService;
	
	@Autowired
	private IbCachedObjectManager cachedObjectManager;
	
	@Autowired
	private PropertiesManager propertiesManager;
	
	@Autowired
	protected TaskTypeDao taskTypeDao;
	
	@Autowired
	private TaskDao taskDao;
	
	@Autowired
	private TransferSao sao;
	
	@Autowired
	private TransferDao transferDao;
	
	@Autowired
	private TransferDomDao transferDomDao;
	
	@Autowired
	protected MassTrfDao massDao;
	
	@Autowired
	protected MassTrfErrorDetailDao massTrfErrorDetailDao;
	
	private Map<String, TaskService> services = new HashMap<String, TaskService>();
	private Map<String, ExecutionDao> daos = new HashMap<String, ExecutionDao>();
	
	public MassTransferSaoImpl() {
		if ( transferService!=null ) {
			services.put(transferService.getTaskType().getTaskType(), transferService);	
			daos.put( transferService.getTaskType().getTaskType(), transferDao );
		} 
		if ( domesticService != null ) {
			services.put( domesticService.getTaskType().getTaskType(), domesticService );		
			daos.put( domesticService.getTaskType().getTaskType(), transferDomDao );
		}
		if ( realtimeService != null ) {
			services.put( realtimeService.getTaskType().getTaskType(), realtimeService );		
			daos.put( realtimeService.getTaskType().getTaskType(), transferDomDao );
		}
		
		logger.debug( "afterPropertieSet(): finished!" );
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		if ( transferService!=null ) {
			services.put(transferService.getTaskType().getTaskType(), transferService);	
			daos.put( transferService.getTaskType().getTaskType(), transferDao );
		} 
		if ( domesticService != null ) {
			services.put( domesticService.getTaskType().getTaskType(), domesticService );		
			daos.put( domesticService.getTaskType().getTaskType(), transferDomDao );
		}
		if ( realtimeService != null ) {
			services.put( realtimeService.getTaskType().getTaskType(), realtimeService );		
			daos.put( realtimeService.getTaskType().getTaskType(), transferDomDao );
		}
	}
	
	@Override
	public Transfer parseMassTrfFile(User user, Information info, Transfer transfer) {
		ApplicationMessage appMessage = createMessage(user, info);
		appMessage.getRequest().addByClass(transfer);
		
		massTransferService.getForMassTransferFile(appMessage);
		
		return appMessage.getResponse().getByClass(Transfer.class);
	}
	
	@Override
	public Transfer createMassTrf(User user, Information info, Transfer transfer) {
		ApplicationMessage message = createMessage(user, info);
		message.getRequest().adds(user, transfer);
		List<Transfer> ceheckFileName = massDao.getAvailableFileMassTrf(transfer.getFileName());
		if(ceheckFileName.isEmpty()){
			// activity log
		
			massTransferService.create(message);
			
			message.getRequest().add(GeneralHelper.C_PARAM_REF_TARGET1,
					transfer.getAccount().getAccountNo());
			
			// run mass trx inquiry
			String dec = null, fileName= null, bankCode=null;
			BigDecimal total=BigDecimal.ZERO;
			BigDecimal totalAmountFail=BigDecimal.ZERO;
			int totalTrx = transfer.getMassTrfDetails().size();
			int totalFail = 0;
			TaskService service = null;
			int MYTHREADS = transfer.getMassTrfDetails().size();
			ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
		//	CompletionService<MassTrfDetail> pool = new ExecutorCompletionService<MassTrfDetail>(executor);
			
			List<MassTrfDetail> detailMassTrf = transfer.getMassTrfDetails();
			List<MassTrfDetail> detailMassTrfResult = new ArrayList<MassTrfDetail>();
			List<Transaction> trx = prepareForMassTransferTransaction(detailMassTrf, transfer, user, info);
			List<Future<MassTrfDetail>> list = new ArrayList<Future<MassTrfDetail>>();
			Collection<MassTransferExecutor> collection = new ArrayList<MassTransferExecutor>( );
					
			for (int i = 0; i < detailMassTrf.size(); i++) {
 				fileName = detailMassTrf.get(i).getFileName();
 				dec = detailMassTrf.get(i).getDescription();
 				total=total.add(detailMassTrf.get(i).getAmount());
 				bankCode = detailMassTrf.get(i).getBankCode(); 
 				ApplicationMessage appMessage = new ApplicationMessage();
 				appMessage = createMessage(user, info);
 				appMessage.getRequest().adds(user, trx.get(i));
				addTaskType(appMessage, trx.get(i));
 				appMessage.getRequest().addByClass(trx.get(i));
 				appMessage.getRequest().addByClass( trx.get(i).getTaskType());
 				logger.info("Execute trx inq {}/{}/{}", new Object[]{i,trx.get(i),detailMassTrf.get(i)});
 				MassTransferExecutor task =  new MassTransferExecutor(detailMassTrf.get(i), service, appMessage, trx.get(i), null, GeneralHelper.C_STATUS_POST_INQUIRY_STAGE);
 				collection.add(task);
             }
			
			try {
				list = executor.invokeAll(collection);
				for (Future<MassTrfDetail> future : list) {
					MassTrfDetail massTransfDet = new MassTrfDetail();
					massTransfDet = future.get();
					if (massTransfDet.getStatusTrx() != GeneralHelper.C_STATUS_PENDING_EXECUTE) {
						massTransfDet.setBankCode(bankCode);
						totalFail++;
						totalAmountFail = totalAmountFail.add(massTransfDet.getAmount());
					}
					detailMassTrfResult.add(massTransfDet);
				}
			} catch (InterruptedException e) {
				logger.error("Exception caught!",e);
			} catch (ExecutionException e) {
				logger.error("Exception caught!",e);
			}
            executor.shutdown();

            transfer.setFileName(fileName);
			transfer.setMassDec(dec);
			transfer.setTotalRecord(totalTrx-totalFail);
			transfer.setTrxCountAll(totalTrx);
			transfer.setTrxTotal(total.subtract(totalAmountFail));
			transfer.setTrxTotal2(total);
			transfer.setMassTrfDetails(detailMassTrfResult);
			message.getResponse().addByClass(transfer);
			
			return message.getResponse().getByClass(Transfer.class);
		}else{
			ValidationException ve=new ValidationException("File name already use");
			ve.setErrorCode(MessageCodes.ERR_ME_FILE_NAME_ALREADY_USE);
			throw ve;
		}
	}
	
	@Override
	public Transfer massTrfExecution(User user, Information info,
			Transfer transfer, String refId, String mPassCode) {
		
			ApplicationMessage message = createMessage(user, info);
			MessagesContainer req = message.getRequest();
			req.adds(user, transfer);
			addTokenInformationToRequest(message, refId, mPassCode);
			addTaskType(message,transfer);
			
			massTransferService.queueExecution(message);
			
			// activity log
			req.add(GeneralHelper.C_PARAM_REF_TARGET1, transfer.getAccount()
					.getAccountNo());

		
			//insert task mass trf 
			massDao.insertTask(transfer);
			
			//skip validation mpin
			
			// run mass trx
			String dec = null, fileName= null, bankCode=null;
			BigDecimal totalAmountFail=BigDecimal.ZERO;
			BigDecimal total=BigDecimal.ZERO;
			int totalTrx = transfer.getMassTrfDetails().size();
			int totalFail = 0;
			int count = 0;
			long refId2 =transfer.getRefId();
			String status = "";
			TaskService service = null;
			int MYTHREADS = transfer.getMassTrfDetails().size();
			ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
			CompletionService<MassTrfDetail> pool = new ExecutorCompletionService<MassTrfDetail>(executor);
			
			List<MassTrfDetail> detailMassTrf = transfer.getMassTrfDetails();
			List<MassTrfDetail> detailMassTrfResult = new ArrayList<MassTrfDetail>();
			List<Transaction> trx = prepareForMassTransferTransaction(detailMassTrf, transfer, user, info);
			List<Future<MassTrfDetail>> list = new ArrayList<Future<MassTrfDetail>>();
			Collection<MassTransferExecutor> collection = new ArrayList<MassTransferExecutor>( );
			
			for (int i = 0; i < detailMassTrf.size(); i++) {
		
 				fileName = detailMassTrf.get(i).getFileName();
 				dec = detailMassTrf.get(i).getDescription();
 				total=total.add(detailMassTrf.get(i).getAmount());
 				bankCode = detailMassTrf.get(i).getBankCode(); 
 				ApplicationMessage appMessage = new ApplicationMessage();
				appMessage = createMessage(user, info);
				appMessage.getRequest().adds(user, trx.get(i));
				addTaskType(appMessage, trx.get(i));
				appMessage.setSkipValidation(true);
				appMessage.getRequest().addByClass(trx.get(i));
				appMessage.getRequest().addByClass( trx.get(i).getTaskType());
				logger.info("Execute trx confrim is {}/{}/{}", new Object[]{i,trx.get(i),detailMassTrf.get(i)});
				MassTransferExecutor task =  new MassTransferExecutor(detailMassTrf.get(i), service, appMessage, trx.get(i), refId2, GeneralHelper.C_STATUS_POST_CONFIRMATION_STAGE);
				collection.add(task);
             }
			
			try {
				list = executor.invokeAll(collection);
				
				for (Future<MassTrfDetail> future : list) {
					MassTrfDetail massTransfDet = new MassTrfDetail();
					massTransfDet = future.get();
					logger.info("## massTransfDet status transaksi" + massTransfDet);
					if (massTransfDet.getTransferType() == GeneralHelper.C_TRANSFER_TYPE_INTERNAL) {
						if (count > 0)
							status += ",";
						status += future.get().getStatusTrx();
						count++;
					}
					if (massTransfDet.getStatusTrx() != GeneralHelper.C_CREATE_SUCCESS) {
						totalFail++;
						totalAmountFail = totalAmountFail.add(massTransfDet.getAmount());
					}
					massTransfDet.setBankCode(bankCode);
					detailMassTrfResult.add(massTransfDet);
	
				}
			} catch (InterruptedException e) {
				logger.error("Exception caught!",e);
			} catch (ExecutionException e) {
				logger.error("Exception caught!",e);
			} 
            
			executor.shutdown();
             
			transfer.setFileName(fileName);
			transfer.setMassDec(dec);
			transfer.setTotalRecord(totalTrx-totalFail);
			transfer.setTrxCountAll(totalTrx);
			transfer.setTrxTotal(total.subtract(totalAmountFail));
			transfer.setTrxTotal2(total);
			transfer.setMassTrfDetails(detailMassTrfResult);
			
			if (status.contains(GeneralHelper.C_ATTR_STATUS_SUCCESS) &&
					status.contains(GeneralHelper.C_ATTR_STATUS_FAIL)){
				massDao.updateStatusTfMass(refId2, GeneralHelper.C_STATUS_GENERAL);
			}else if (status.contains(GeneralHelper.C_ATTR_STATUS_FAIL)){
				massDao.updateStatusTfMass(refId2, GeneralHelper.C_STATUS_FAIL);
			}
			message.setRefNo(transfer.getRefNo());
			message.getResponse().addByClass(transfer);
			
			return message.getResponse().getByClass(Transfer.class);
			
	}
	
	private List<Transaction> prepareForMassTransferTransaction(List<MassTrfDetail> detailMassTrf, Transfer transfer, User user, Information info ){
		Transaction trx;
		List<Transaction> massTrfTransactionResult = new ArrayList<Transaction>();
		for (MassTrfDetail  massTrf: detailMassTrf){
			trx = getTrxObject(massTrf, transfer, user, info);
			massTrfTransactionResult.add(trx);
		}
		return massTrfTransactionResult;
	}
	
	private Transaction getTrxObject(MassTrfDetail massTrf, Transfer transfer, User user, Information info){
		return addElement(massTrf, transfer.getAccount() , user, info);
	}
	
	private BaseTransfer addElement(MassTrfDetail massTrf, AccountInformation act, User user, Information info){
		logger.info(">> case :"+massTrf.getTransferType());
		TaskType ttype;
		BaseFee bfee = new BaseFee();
		switch(massTrf.getTransferType()){
		case GeneralHelper.C_TRANSFER_TYPE_INTERNAL:
			Transfer transfer = new Transfer();
			transfer.setRefNo(massTrf.getRefNo());
			transfer.setRefId(massTrf.getRefId());
			transfer.setUserId(user.getUserId());
			transfer.setAccount( act );
			transfer.setAmount( massTrf.getAmount() );
			transfer.setToName(massTrf.getToName());
			transfer.setCurrencyCode( GeneralHelper.C_CURRENCY_CODE_IDR );
			transfer.setToAccountNo( massTrf.getToAccount().getAccountNo() );
			transfer.setMessage1( massTrf.getMessage1() );
			transfer.setTrxType(GeneralHelper.C_TRX_TYPE_INDIVIDUAL);
			transfer.setExecutionType(GeneralHelper.C_TRX_IMMEDIATE);
			transfer.setToCurrency(cachedObjectManager.getCurrency(GeneralHelper.C_CURRENCY_IDR));
			
			Currency currency = new Currency();
			currency.setCurrencyCode(transfer.getAccount().getCurrencyCode());
			transfer.setCurrency(currency);
			
			//convert to amount local (note : currency code valid only in phase 1A or same currency. Need to review later)
			ExchangeRates er = sao.getExchangeRate(user, info, transfer.getAccount().getCurrencyCode());
			logger.info( "curCode internal...: [{}], er: [{}]", new Object[] { transfer.getAccount().getCurrencyCode(), er} );
			
			transfer.setRateExchange(BigDecimal.ONE);
			if (er != null)
				transfer.setRateExchange(er.getBnBuy());
			transfer.setAmountLocal(transfer.getAmount().multiply(transfer.getRateExchange()));
				
			ttype = taskTypeDao.getDetail(massTrf.getTaskType().getTaskType());
			transfer.setTaskType( ttype );
			transfer.setDateTrx(new Date());
			
			if(isVAAccount(massTrf.getToAccount().getAccountNo()) 
    				&& !transfer.getAccount().getCurrencyCode().equals(GeneralHelper.C_CURRENCY_IDR)){
    			ValidationException ve=new ValidationException("Source Account must IDR if destination is va account");
    			ve.setErrorCode(MessageCodes.ERR_ME_ACCOUNT_FORBIDDEN);
    			throw ve;
    		}
			
			//-> new charge amount
			bfee.setCharge(massTrf.getCharge());
			transfer.setBaseFee(bfee);
			transfer.setDateCreate(new Date());
			transfer.setMassTrfType(massTrf.getTransferType());
			
			logger.info("addElement transfer result: {}", transfer);
			
			return transfer;
		case GeneralHelper.C_TRANSFER_TYPE_SKN:
		case GeneralHelper.C_TRANSFER_TYPE_RTGS:
			TransfersDom transferDom = new TransfersDom();
			transferDom.setRefNo(massTrf.getRefNo());
			transferDom.setRefId(massTrf.getRefId() );
			transferDom.setUserId(user.getUserId());
			transferDom.setAccount( act );
			transferDom.setAmountLocal( massTrf.getAmount() );
			transferDom.setAmount( massTrf.getAmount() );
			transferDom.setCurrencyCode(GeneralHelper.C_CURRENCY_CODE_IDR);
			transferDom.setToAccountNo( massTrf.getToAccount().getAccountNo() );
			transferDom.setMessage( massTrf.getMessage1() );
			transferDom.setToName(massTrf.getToName());
			transferDom.setBnfBank(massTrf.getBnfBank());
			transferDom.setResidence(massTrf.getResidence());
			transferDom.setNationality(massTrf.getNationality());
			transferDom.setTransferType(massTrf.getTransferType());
			transferDom.setToCurrency(cachedObjectManager.getCurrency(GeneralHelper.C_CURRENCY_IDR));
			transferDom.setCustomerType(massTrf.getCustomerType());
			transferDom.setMassTrfType(massTrf.getTransferType());
			
			currency = new Currency();
			currency.setCurrencyCode(transferDom.getAccount().getCurrencyCode());
			transferDom.setCurrency(currency);
			
			transferDom.setTrxType(GeneralHelper.C_TRX_TYPE_INDIVIDUAL); 
			transferDom.setExecutionType(GeneralHelper.C_TRX_IMMEDIATE);
			
			ttype = taskTypeDao.getDetail(massTrf.getTaskType().getTaskType());
			transferDom.setTaskType( ttype );
			
			//convert to amount local (note : currency code valid only in phase 1A or same currency. Need to review later)
		    er = sao.getExchangeRate(user, info, transferDom.getAccount().getCurrencyCode());
			logger.info( "~~ curCode domestic...: [{}], er: [{}]", new Object[] { transferDom.getAccount().getCurrencyCode(), er} );
			
			transferDom.setRateExchange(BigDecimal.ONE);
			if (er != null)
				transferDom.setRateExchange(er.getBnBuy());
			transferDom.setAmountLocal(transferDom.getAmount().multiply(transferDom.getRateExchange()));
			
			//-> new charge amount
			bfee.setCharge(massTrf.getCharge());
			transferDom.setBaseFee(bfee);
			
			transferDom.setDateCreate(new Date());
			transferDom.setDateTrx(new Date());

			logger.info("addElement transferDom result: {}", transferDom);
			
			return transferDom;
		case GeneralHelper.C_TRANSFER_TYPE_REALTIME:
			transferDom = new TransfersDom();
			transferDom.setRefNo(massTrf.getRefNo());
			transferDom.setRefId(massTrf.getRefId() );
			transferDom.setUserId(user.getUserId());
			transferDom.setAccount( act );
			transferDom.setAmountLocal( massTrf.getAmount() );
			transferDom.setAmount( massTrf.getAmount() );
			transferDom.setCurrencyCode(GeneralHelper.C_CURRENCY_CODE_IDR);
			transferDom.setToAccountNo( massTrf.getToAccount().getAccountNo() );
			transferDom.setMessage( massTrf.getCustRef() );
			transferDom.setMessageRefNo(massTrf.getCustRef().toString());
			transferDom.setBnfBank(massTrf.getBnfBank());
			transferDom.setToCurrency(cachedObjectManager.getCurrency(GeneralHelper.C_CURRENCY_IDR));
			transferDom.setTransferType(massTrf.getTransferType());
			transferDom.setMassTrfType(massTrf.getTransferType());
			transferDom.setMessageRefNo(massTrf.getCustRef());
			transferDom.setCurrency(transferDom.getAccount().getProduct().getCurrency());
			transferDom.setExecutionType(GeneralHelper.C_TRX_IMMEDIATE); 
			transferDom.setTrxType(GeneralHelper.C_TRX_TYPE_INDIVIDUAL); 
			transferDom.setToName(massTrf.getToName());
			transferDom.setStan(massTrf.getStan());
			transferDom.setPrimaryAccountNumber(massTrf.getPrimaryAccountNumber());
			transferDom.setProcessingCode(massTrf.getProcessingCode());
			
			ttype = taskTypeDao.getDetail(massTrf.getTaskType().getTaskType());
			transferDom.setTaskType( ttype );
			
			BigDecimal feeRealtime =null;
			BaseFee fee = new BaseFee();
			String temp = propertiesManager.getProperty(ParameterHelper.constructPropertyKey(massTrf.getTaskType().getTaskType(), GeneralHelper.C_Fee_REALTIME_Transaction));
			
			if(temp!=null)  feeRealtime = new BigDecimal(temp);
			fee.setCharge(feeRealtime );
			transferDom.setBaseFee(fee);
			
			transferDom.setDateCreate(new Date());
			transferDom.setDateTrx(new Date());

			logger.info("addElement online transfer result: {}", transferDom);
			
			return transferDom;
		}
		return null;
	}
	
	public class MassTransferExecutor implements Callable<MassTrfDetail> {
		public ApplicationMessage message;
		public MassTrfDetail  massTrf;
		public TaskService service;
		public Transaction trx;
		public Long refId;
		public Byte statusTrx;
		
		MassTransferExecutor( MassTrfDetail trxResult, TaskService service, ApplicationMessage message, Transaction trx, Long refId, Byte statusTrx) {
			this.service = service;
			this.message = message;
			this.massTrf = trxResult;
			this.trx = trx;
			this.refId = refId;
			this.statusTrx = statusTrx;
		}
		
		@Override
        public MassTrfDetail call() throws Exception {
			service = services.get(trx.getTaskType().getTaskType());
			logger.info(" ## status {}##", new Object[]{massTrf.getStatusTrx()});
			if (statusTrx == GeneralHelper.C_STATUS_POST_INQUIRY_STAGE){
				logger.info(" ## status inq {}/{}##", new Object[]{massTrf.getBankRefNo(),massTrf.getStatusTrx()});
           	 	if ( massTrf.getStatusTrx() == GeneralHelper.C_STATUS_PENDING_EXECUTE )	{
           	 		message =  service.create(message);
           	 		
					MessagesContainer res = message.getResponse();
					if (res != null){
						refId = res.get(GeneralHelper.C_PARAM_REF_TARGET1);
						massTrf.setRefId(refId);
						massTrf.setRefNo(message.getRefNo());
						if (massTrf.getTaskType().getTaskType().equals(GeneralHelper.C_TASK_TYPE_TRANSFER_WITHIN_BANK)){
							Transfer trf = res.getByClass(Transfer.class);
							massTrf.setToName(trf.getToName());
						}else if (massTrf.getTaskType().getTaskType().equals(GeneralHelper.C_TASK_TYPE_TRANSFER_SKN_RTGS)){
							TransfersDom trfDom = res.getByClass(TransfersDom.class);
							massTrf.setResidentCountry(trfDom.getResidentCountry());
						}else if (massTrf.getTaskType().getTaskType().equals(GeneralHelper.C_TASK_TYPE_TRANSFER_REALTIME)){
							TransfersDom trfDom = res.getByClass(TransfersDom.class);
							massTrf.setStan(trfDom.getStan());
							massTrf.setProcessingCode(trfDom.getProcessingCode());
							massTrf.setPrimaryAccountNumber(trfDom.getPrimaryAccountNumber());
							massTrf.setToName(trfDom.getToName());
						}
						
						if (message.getStatus() != GeneralHelper.C_STATUS_SUCCESS){
							massTrf.setStatusTrx(message.getStatus());
							massTrf.setResponseCodeTrx(message.getResultCode());
							MassTrfErrorDetail er = massTrfErrorDetailDao.getErrorDetail(message.getResultCode());
							if(er != null){
									massTrf.setErrorWordingI(er.getWordingI());
									massTrf.setErrorWordingE(er.getWordingE());
							}
						}
					}
	           	 }else{
						MassTrfErrorDetail er = massTrfErrorDetailDao.getErrorDetail(massTrf.getResponseCodeTrx());
						if(er != null){
		  					massTrf.setErrorWordingI(er.getWordingI());
		  					massTrf.setErrorWordingE(er.getWordingE());
						}
        	 	}
			}else{
				 if ( massTrf.getStatusTrx() == GeneralHelper.C_STATUS_PENDING_EXECUTE )	{
						message =  service.queueExecution(message);
						logger.info(" ## status, resultCode {}/{} ##", new Object[]{message.getStatus(),message.getResultCode()});
						massTrf.setStatusTrx(message.getStatus());
						massTrf.setResponseCodeTrx(message.getResultCode());
				 }
			}
			return massTrf;
		}
	}
	
	private boolean isVAAccount(String account){
		String [] array = propertiesManager.getProperty(QNBGeneralHelper.C_KEY_QVA_PREFIX).split(GeneralHelper.C_SEMICOLON_DELIMITER);
		
		for (int i = 0; i < array.length; i++) {
			if(account.startsWith(array[i]))
				return true;
		}
		
		return false;
	}
}
*/