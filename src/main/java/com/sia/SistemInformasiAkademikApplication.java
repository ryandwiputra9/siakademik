package com.sia;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.sia.dao")
public class SistemInformasiAkademikApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemInformasiAkademikApplication.class, args);
		
		System.out.println("OS NAME : "+System.getProperty("os.name"));
		System.out.println("OS VERSION : "+System.getProperty("os.version"));
		System.out.println("OS ARCITECTURE : "+System.getProperty("os.arch"));
		System.out.println("JAVA VERSION : "+System.getProperty("java.version"));
		
	}
	
}

