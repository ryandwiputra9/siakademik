package com.sia.util;

import java.util.Random;


import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Helper {

	
	public static String getPasswordGenerator() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	//	bCryptPasswordEncoder.encode(getRandomString());
		return bCryptPasswordEncoder.encode(getRandomString());
	}
	
	

	public static String getRandomString() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 18) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}
}
