package com.sia.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sia.entity.Walikelas;
import com.sia.services.WalikelasService;

@RestController
@RequestMapping("/api")
public class restWalikelas {

	private final Logger logger = Logger.getLogger(getClass().getName());
	@Autowired
	private WalikelasService service;

	@GetMapping(value = "/walikelas", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Walikelas>> getAll() {
		List<Walikelas> Walikelas = service.getListWalikelas();
		if (!Walikelas.isEmpty()) {
			return new ResponseEntity<List<Walikelas>>(Walikelas, HttpStatus.OK);//
			// ResponseEntity.ok(products);
		}

		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(Walikelas);
	}
	@GetMapping(value = "/dummy/walikelas", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Walikelas>> getDummyWalikelas() {
		List<Walikelas> Walikelas = service.getListWalikelasDummy();
		if (!Walikelas.isEmpty()) {
			return new ResponseEntity<List<Walikelas>>(Walikelas, HttpStatus.OK);//
		}
		return new HttpEntity<>(Walikelas);
	}

	@GetMapping(value = "/walikelas/search", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Walikelas>> searchByNameOrId(@RequestParam(required = false) String id,
			@RequestParam(required = false) String nama) {
		logger.info("Search  ID :" + id + " Nama : " + nama);
		Walikelas s = new Walikelas();
		s.setId_walikelas(id);
		s.setNama(nama);
		List<Walikelas> Walikelas = service.getListByNameOrId(s);

		if (!Walikelas.isEmpty())
			return new ResponseEntity<List<Walikelas>>(Walikelas, HttpStatus.OK);//
		else
			return new ResponseEntity<List<Walikelas>>(Walikelas, HttpStatus.NO_CONTENT);//

	}

	/*
	 * Note jika dengan @PathVariable localhost:9999/api/Walikelas/1 jika
	 * dengan @RequestParam localhost:9999/api/Walikelas/?id=1
	 * 
	 */

	// @GetMapping(value = "/Walikelas/{id}") with pathVariable
	@GetMapping(value = "/walikelas/") // localhost:9999/api/Walikelas/?id=1
	public HttpEntity<Walikelas> getById(// @PathVariable("id") String id
			@RequestParam(required = false) String id

	) {

		logger.info("getById :" + id);
		Walikelas input = new Walikelas();
		input.setId_walikelas(id);
		Walikelas Walikelas = service.getWalikelasById(input);
		if (Walikelas == null) {
			return new ResponseEntity<Walikelas>(Walikelas, HttpStatus.NO_CONTENT);// ResponseEntity.ok(products);
		}

		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(Walikelas);
	}

	@PostMapping(value = "/walikelas")
	@ResponseStatus(HttpStatus.CREATED)
	public void AddWalikelas(@RequestBody Walikelas s) {
		logger.info("Add Walikelas : " + s);
		service.insertWalikelas(s);
	}

	@PutMapping(value = "/walikelas")
	@ResponseStatus(HttpStatus.OK)
	public List<Walikelas> UpdateWalikelas(@RequestBody Walikelas s) {
		logger.info("Update Walikelas : " + s);

		/* reload List all Walikelas */
		List<Walikelas> list = service.updateWalikelas(s);
		return list;
	}

	@DeleteMapping(value = "/walikelas")
	@ResponseStatus(HttpStatus.OK)
	public void DeleteWalikelas(@RequestBody Walikelas s) {
		logger.info("Delete Walikelas : " + s);
		service.deleteWalikelas(s);
	}

	/*
	 * @GetMapping(value = "/Walikelas/xml/", produces = {
	 * MediaType.APPLICATION_XML_VALUE }) public ResponseEntity<List<Walikelas>>
	 * xml() { List<Walikelas> products = service.getListWalikelas();
	 * logger.info("<<<Loginf>>>"); logger.info("Logginf : " + products); return new
	 * ResponseEntity<List<Walikelas>>(products, HttpStatus.OK); }
	 */
	@GetMapping(value = "/Walikelas/xml/", produces = { MediaType.APPLICATION_XML_VALUE }) // localhost:9999/api/Walikelas/?id=1
	public HttpEntity<ArrayList<Walikelas>> getByIdXml(// @PathVariable("id") String id
			@RequestParam(required = false) String id, @RequestParam(required = false) String nama) {

		logger.info("Search  ID :" + id + " Nama : " + nama);
		Walikelas input = new Walikelas();
		input.setId_walikelas(id);
		input.setNama(nama);
		ArrayList<Walikelas> Walikelas = service.getListByNameOrId(input);
		if (Walikelas.isEmpty()) {
			return new ResponseEntity<ArrayList<Walikelas>>(Walikelas, HttpStatus.NO_CONTENT);// ResponseEntity.ok(products);
		}
		return new HttpEntity<>(Walikelas);
	}

}
