package com.sia.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sia.entity.Siswa;
import com.sia.services.CustomUserDetailsService;
import com.sia.services.SiswaService;
import com.sia.services.UserDetails;

@RestController
@RequestMapping("/api")
public class restSiswa extends UserDetails{
//	 private final Logger logger = Logger.getLogger(getClass().getName());
	// private final Logger logger = Logger.getLogger(getClass().getName());
//	private final Logger logger = Logger.getLogger(this.getClass().getName());
//	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private SiswaService service;

	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/siswa", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Siswa>> getAll() {
		List<Siswa> Siswa = service.getListSiswa();
		if (!Siswa.isEmpty()) {
			return new ResponseEntity<List<Siswa>>(Siswa, HttpStatus.OK);//
			// ResponseEntity.ok(products);
		}
		logger.info("Logger CUY {} ");
		System.out.println("USER ID : "+getUser().getId());
		System.out.println("USER Name : "+getUser().getUsername());
		logger.info("USER Name : {}",getUser().getUsername());
	//	logger.debug("Logger ALL SISWA : " + Siswa);
		logger.debug("Logger ALL SISWA : {}" , Siswa);
		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(Siswa);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/siswa/search", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<Siswa>> searchByNameOrId(@RequestParam(required = false) String id,
			@RequestParam(required = false) String nama) {
		logger.info("Search  ID :" + id + " Nama : " + nama);
		Siswa s = new Siswa();
		s.setId(id);
		s.setNama(nama);
		List<Siswa> Siswa = service.findByNameOrId(s);

		if (!Siswa.isEmpty())
			return new ResponseEntity<List<Siswa>>(Siswa, HttpStatus.OK);//
		else
			return new ResponseEntity<List<Siswa>>(Siswa, HttpStatus.NO_CONTENT);//

	}

	/*
	 * Note jika dengan @PathVariable localhost:9999/api/siswa/1 jika
	 * dengan @RequestParam localhost:9999/api/siswa/?id=1
	 * 
	 */

	// @GetMapping(value = "/siswa/{id}") with pathVariable
	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/siswa/") // localhost:9999/api/siswa/?id=1
	public HttpEntity<Siswa> getById(// @PathVariable("id") String id
			@RequestParam(required = false) String id

	) {

		logger.info("getById :" + id);
		Siswa input = new Siswa();
		input.setId(id);
		Siswa siswa = service.getSiswaById(input);
		if (siswa == null) {
			return new ResponseEntity<Siswa>(siswa, HttpStatus.NO_CONTENT);// ResponseEntity.ok(products);
		}

		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(siswa);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@PostMapping(value = "/siswa")
	@ResponseStatus(HttpStatus.CREATED)
	public void AddSiswa(@RequestBody Siswa s) {
		logger.info("Add SISWA : " + s);
		service.insertSiswa(s);
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@PutMapping(value = "/siswa")
	@ResponseStatus(HttpStatus.OK)
	public List<Siswa> UpdateSiswa(@RequestBody Siswa s) {
		logger.info("Update SISWA : " + s);

		/* reload List all siswa */
		List<Siswa> list = service.updateSiswa(s);
		return list;
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@DeleteMapping(value = "/siswa")
	@ResponseStatus(HttpStatus.OK)
	public void DeleteSiswa(@RequestBody Siswa s) {
		logger.info("Delete SISWA : " + s);
		service.deleteSiswa(s);
	}

	/*
	 * @GetMapping(value = "/siswa/xml/", produces = {
	 * MediaType.APPLICATION_XML_VALUE }) public ResponseEntity<List<Siswa>> xml() {
	 * List<Siswa> products = service.getListSiswa(); logger.info("<<<Loginf>>>");
	 * logger.info("Logginf : " + products); return new
	 * ResponseEntity<List<Siswa>>(products, HttpStatus.OK); }
	 */
	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/siswa/xml/", produces = { MediaType.APPLICATION_XML_VALUE }) // localhost:9999/api/siswa/?id=1
	public HttpEntity<ArrayList<Siswa>> getByIdXml(// @PathVariable("id") String id
			@RequestParam(required = false) String id, @RequestParam(required = false) String nama) {

		logger.info("Search  ID :" + id + " Nama : " + nama);
		Siswa input = new Siswa();
		input.setId(id);
		input.setNama(nama);
		ArrayList<Siswa> siswa = service.findByNameOrId(input);
		if (siswa.isEmpty()) {
			return new ResponseEntity<ArrayList<Siswa>>(siswa, HttpStatus.NO_CONTENT);// ResponseEntity.ok(products);
		}
		return new HttpEntity<>(siswa);
	}

}
