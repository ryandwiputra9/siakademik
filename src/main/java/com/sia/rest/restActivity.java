package com.sia.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sia.entity.ActivityLog;
import com.sia.entity.Siswa;
import com.sia.services.ActivityService;
import com.sia.services.CustomUserDetailsService;
import com.sia.services.SiswaService;
import com.sia.services.UserDetails;

@RestController
@RequestMapping("/api")
public class restActivity extends UserDetails{
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private ActivityService Actservice;

	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/activity", produces = { MediaType.APPLICATION_JSON_VALUE })
	public HttpEntity<List<ActivityLog>> getAll() {
		List<ActivityLog> act = Actservice.getAllActivityUser();
		if (!act.isEmpty()) {
			return new ResponseEntity<List<ActivityLog>>(act, HttpStatus.OK);//
		}
		return new HttpEntity<>(act);
	}

	

	/*
	 * Note jika dengan @PathVariable localhost:9999/api/siswa/1 jika
	 * dengan @RequestParam localhost:9999/api/siswa/?id=1
	 * 
	 */

	// @GetMapping(value = "/siswa/{id}") with pathVariable
/*	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/siswa/") // localhost:9999/api/siswa/?id=1
	public HttpEntity<Siswa> getById(// @PathVariable("id") String id
			@RequestParam(required = false) String id

	) {

		logger.info("getById :" + id);
		Siswa input = new Siswa();
		input.setId(id);
		Siswa siswa = service.getSiswaById(input);
		if (siswa == null) {
			return new ResponseEntity<Siswa>(siswa, HttpStatus.NO_CONTENT);// ResponseEntity.ok(products);
		}

		// return new ResponseEntity(HttpStatus.NOT_FOUND);
		return new HttpEntity<>(siswa);
	}*/

}
