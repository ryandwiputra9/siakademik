package com.sia.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.sia.entity.Email;
import com.sia.entity.Metrics;
import com.sia.services.EmailService;

import freemarker.template.TemplateException;

@RestController
public class restMetricts {
	// @Autowired
	// RestTemplate restTemplate;
	@Value("${url.metrics}")
	private String urlMetrics;
	@Autowired
	private EmailService emailService;

	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/dashboard", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Metrics> infoDasboard() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Metrics> responseEntity = restTemplate.getForEntity(urlMetrics, Metrics.class);
		Metrics objects = responseEntity.getBody();
		MediaType contentType = responseEntity.getHeaders().getContentType();
		HttpStatus statusCode = responseEntity.getStatusCode();
		System.out.println("Total Memory :  " + objects.getMem() + "KB");
		System.out.println("Free Memory :  " + objects.getMem_free() + "KB");

		System.out.println("Free Ram :  " + objects.getFreeMemPercent() + " %");
		System.out.println("USED Ram :  " + objects.getUsedMemPercent() + " %");
		return responseEntity;
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/sendEmail", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String sendEmail() {
		Email mail = new Email();
		mail.setFrom("lx.linux64@gmail.com");
		mail.setTo("ryanlx32@gmail.com");
		mail.setSubject("Sending Email with Thymeleaf HTML Template Example");

		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("name", mail.getFrom());
		model.put("location", "Belgium");
		model.put("mail", mail.getSubject());
		model.put("message", "Testing");
		mail.setModel(model);

		try {
			emailService.sendSimpleMessage(mail);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "Send Email";
	}

	@PreAuthorize("hasAnyRole('ROLE_ADMIN') or hasAnyRole('ROLE_SUPER_ADMIN')")
	@GetMapping(value = "/sendEmailFreemaker", produces = { MediaType.APPLICATION_JSON_VALUE })
	public String sendEmailFreemaker() {

		Email mail = new Email();
		mail.setFrom("lx.linux64@gmail.com");
		mail.setTo("ryanlx32@gmail.com");
		mail.setSubject("Sending Email with Freemarker HTML Template Example");

		Map model = new HashMap();
		model.put("name", "Ryan D Putra");
		model.put("location", "Jakarta");
		model.put("signature", "https://ryand.com");
		mail.setModel(model);

		try {
			emailService.sendSimpleMessageFreemaker(mail);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "freemaker";
	}

	public static void main(String[] args) throws IOException {
		/* Native */
		String url = "http://localhost:9999/metrics";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		// optional default is GET
		con.setRequestMethod("GET");
		// add request header
		// con.setRequestProperty("User-Agent", "Mozilla/5.0");
		// con.setRequestProperty("X-Nixcenter-Token", "DEPOKDEPOKDEPOK");
		// int responseCode = con.getResponseCode();
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		System.out.println(" Respone " + response.toString());
		// return response.toString();

		/* with rest Template */

		/*
		 * RestTemplate restTemplate = new RestTemplate(); ResponseEntity<Metrics>
		 * responseEntity = restTemplate.getForEntity(url, Metrics.class); Metrics
		 * objects = responseEntity.getBody(); MediaType contentType =
		 * responseEntity.getHeaders().getContentType(); HttpStatus statusCode =
		 * responseEntity.getStatusCode(); System.out.println("Total Memory :  " +
		 * objects.getMem() + ""); System.out.println("Free Memory :  " +
		 * objects.getMem_free());
		 * 
		 * int totalMem = Integer.valueOf(objects.getMem()); int freeMem =
		 * Integer.valueOf(objects.getMem_free()); int usedMem = totalMem - freeMem;
		 * //513031 System.out.println("Free Ram : "+freeMem / 1024);
		 * System.out.println("Used Ram : "+usedMem /1024);
		 */
		// int convertToMb=
		float total = 513031;

		float free = 200483;
		float used = total - free;

		System.out.println("Free Ram :  " + (free * 100) / total + " %");
		System.out.println("USED Ram :  " + (used * 100) / total + " %");
		System.out.println();

		// System.out.println("Used Mem : " + usedMem + "KB = " + (usedMem / 1024) + "
		// MB ");

	}

}
