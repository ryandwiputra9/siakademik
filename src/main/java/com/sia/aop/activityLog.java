package com.sia.aop;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.sia.dao.activityDao;
import com.sia.entity.ActivityLog;
import com.sia.services.CustomUserDetailsService;

@Aspect
@Configuration
public class activityLog {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private CustomUserDetailsService cud;
	@Autowired
	private activityDao actDao;
	/*
	 * @Before("execution(* com.in28minutes.springboot.tutorial.basics.example.aop.data.*.*(..))"
	 * ) public void before(JoinPoint joinPoint) { //Advice
	 * logger.info(" Check for user access ");
	 * logger.info(" Allowed execution for {}", joinPoint); }
	 */
	@Autowired
	private HttpServletRequest request;
	@After(value = "execution(* com.sia.rest.*.*(..))")
	public void after(JoinPoint joinPoint) {
		ActivityLog actLog =new ActivityLog();
		actLog.setUserId(cud.getUser().getId());
		//actLog.setActivityName(joinPoint.getTarget().getClass().getSimpleName());
		//actLog.setActivityType(activityType);
		
		actLog.setModuleName(joinPoint.getTarget().getClass().getSimpleName());
		actLog.setModuleCode(joinPoint.getSignature().getName());
		actLog.setUserAgent(request.getHeader("User-Agent"));
		actLog.setIpAddress(request.getRemoteHost());
		logger.info("after execution rest controller of {}", joinPoint.getTarget().getClass().getSimpleName());
		logger.info("after execution rest controller of 2 {}", joinPoint.getSignature().getName());
		logger.info("LOCALE {}", request.getLocale());
		logger.info("SESSION {}", request.getSession());
		logger.info("USER PRINCIPAL {}", request.getUserPrincipal());
		logger.info("IP ADDRESS CLIENT 5 {}", request.getRemoteHost());
		logger.info("IP ADDRESS SERVER {}", request.getLocalAddr());
		logger.info("USER AGENT {}", request.getHeader("User-Agent"));
		logger.info("X-Forwarded-For {}", request.getHeader("X-Forwarded-For"));
		logger.info("Proxy-Client-IP {}", request.getHeader("Proxy-Client-IP"));
		logger.info("WL-Proxy-Client-IP {}", request.getHeader("WL-Proxy-Client-IP"));
		logger.info("HTTP_X_FORWARDED_FOR {}", request.getHeader("HTTP_X_FORWARDED_FOR"));
		logger.info("HTTP_X_FORWARDED {}", request.getHeader("HTTP_X_FORWARDED"));
		logger.info("HTTP_X_CLUSTER_CLIENT_IP {}", request.getHeader("HTTP_X_CLUSTER_CLIENT_IP"));
		logger.info("HTTP_CLIENT_IP {}", request.getHeader("HTTP_CLIENT_IP"));
		logger.info("HTTP_FORWARDED_FOR {}", request.getHeader("HTTP_FORWARDED_FOR"));
		logger.info("HTTP_FORWARDED {}", request.getHeader("HTTP_FORWARDED"));
		logger.info("HTTP_VIA {}", request.getHeader("HTTP_VIA"));
		logger.info("REMOTE_ADDR {}", request.getHeader("REMOTE_ADDR"));
		
		
		actDao.insertActivity(actLog);
	}
}
