package com.sia.dao;

import java.util.List;

import com.sia.entity.Guru;

public interface guruDao {
    void insertGuru(Guru user);
    void updateGuru(Guru user);
    void deleteGuru(Guru user);
    Guru findGuruById(String id);
    List<Guru> findAllGuru();
}
