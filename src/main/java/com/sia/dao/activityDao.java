package com.sia.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sia.entity.ActivityLog;
import com.sia.entity.Siswa;

@Mapper
public interface activityDao {
	public void insertActivity(ActivityLog log);
	public List<ActivityLog> checkAlluserActivity();

}
