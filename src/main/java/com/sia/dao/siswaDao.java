package com.sia.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sia.entity.Siswa;

@Mapper
public interface siswaDao {
	public void insertSiswa(Siswa user);

	public void updateSiswa(Siswa user);

	public void deleteSiswa(Siswa user);

	public Siswa findSiswaById(String id);

	public List<Siswa> findAllSiswa();
	public ArrayList<Siswa> findByNameOrId(Siswa s);
}
