package com.sia.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sia.entity.Walikelas;
@Mapper
public interface walikelasDao {
	public void insertWalikelas(Walikelas wk);

	public void updateWalikelas(Walikelas wk);

	public void deleteWalikelas(Walikelas wk);

	public Walikelas findWalikelasById(Walikelas id);
	// public UserDetails findUserByName(String username);

	public List<Walikelas> findAllWalikelas();

	public ArrayList<Walikelas> getListByNameOrId(Walikelas s);
}
