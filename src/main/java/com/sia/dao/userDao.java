package com.sia.dao;

import java.util.Optional;

import org.apache.ibatis.annotations.Mapper;

import com.sia.entity.Users;

@Mapper
public interface userDao {
	Optional<Users> findByName(String username);

}
