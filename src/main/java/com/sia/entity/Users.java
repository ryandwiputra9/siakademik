package com.sia.entity;

import javax.persistence.*;
import java.util.Set;

//@Entity
//@Table(name = "user")
public class Users {

	// @Id
	// @GeneratedValue(strategy = GenerationType.AUTO)
	// @Column(name = "user_id")
	private int id;
	// @Column(name = "email")
	private String email;
	// @Column(name = "password")
	private String password;
	// @Column(name = "name")
	private String username;
	// @Column(name = "last_name")
	private String fullname;
	// @Column(name = "active")
	private int active;
	// @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	// @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"),
	// inverseJoinColumns = @JoinColumn(name = "role_id"))
	// private Set<Role> roles;
	private Role role;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Users() {
	}

	/*
	 * public Set<Role> getRoles() { return roles; }
	 * 
	 * public void setRoles(Set<Role> roles) { this.roles = roles; }
	 */

	public Users(Users users) {
        this.active = users.getActive();
        this.email = users.getEmail();
        this.role = users.getRole();
        this.username = users.getUsername();
        this.fullname =users.getLastName();
        this.id = users.getId();
        this.password = users.getPassword();
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String name) {
		this.username = name;
	}

	public String getLastName() {
		return fullname;
	}

	public void setLastName(String lastName) {
		this.fullname = lastName;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

}
