package com.sia.entity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
//@XmlRootElement(name = "Siswa")
//@XmlAccessorType(XmlAccessType.NONE)
public class Siswa {

	// private @Getter @Setter String id; lombok
//	@XmlAttribute
	@NotNull
	private String id;
//	@XmlElement
	private String nis;
	@XmlElement
	private String nisn;
	@XmlElement
	private String nama;
	@XmlElement
	private String tanggalLahir;
	@XmlElement
	private String tempatLahir;
	// @XmlElement
	// private String agama;
	@XmlElement
	private String tlp;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNis() {
		return nis;
	}

	public void setNis(String nis) {
		this.nis = nis;
	}

	public String getNisn() {
		return nisn;
	}

	public void setNisn(String nisn) {
		this.nisn = nisn;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getTempatLahir() {
		return tempatLahir;
	}

	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}

	public String getTlp() {
		return tlp;
	}

	public void setTlp(String tlp) {
		this.tlp = tlp;
	}

}
