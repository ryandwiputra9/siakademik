package com.sia.entity;

/*import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;*/

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;



public class CustomUserDetails extends Users implements UserDetails {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomUserDetails(final Users users) {
		super(users);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		System.out.println("ROLE " + this.getRole().getRole_name());
		System.out.println("ROLE ID " + this.getRole().getRole_id());
		Set<String> hash_Set = new HashSet<String>();

		hash_Set.add("role_id" + this.getRole().getRole_id());
		hash_Set.add("role" + this.getRole().getRole_name());
		System.out.println("isCredentialsNonExpired " + this.isCredentialsNonExpired());
		System.out.println("is ENABLED" + this.isEnabled());
		System.out.println("isAccountNonExpired " + this.isAccountNonExpired());
		return

		hash_Set.stream().map(role_name -> new SimpleGrantedAuthority("ROLE_" + this.getRole().getRole_name()))
				.collect(Collectors.toList());
	}

	@Override
	public String getPassword() {
		return super.getPassword();
	}

	@Override
	public String getUsername() {
		return super.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
