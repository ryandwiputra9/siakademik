package com.sia.entity;

import lombok.Data;

@Data
public class Guru {
	private String id_guru;
	private String nip;
	private String nama;
	private String tanggal_lahir;
	private String tempat_lahir;
	private String agama;
	private String tlp;
	private String email;
	private String lulusan;
	private String jabatan; // guru,kepala sekolah, dll
	private String status_guru; // pns, honorer

	public String getId_guru() {
		return id_guru;
	}

	public void setId_guru(String id_guru) {
		this.id_guru = id_guru;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getAgama() {
		return agama;
	}

	public void setAgama(String agama) {
		this.agama = agama;
	}

	public String getTlp() {
		return tlp;
	}

	public void setTlp(String tlp) {
		this.tlp = tlp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLulusan() {
		return lulusan;
	}

	public void setLulusan(String lulusan) {
		this.lulusan = lulusan;
	}

	public String getJabatan() {
		return jabatan;
	}

	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}

	public String getTanggal_lahir() {
		return tanggal_lahir;
	}

	public void setTanggal_lahir(String tanggal_lahir) {
		this.tanggal_lahir = tanggal_lahir;
	}

	public String getTempat_lahir() {
		return tempat_lahir;
	}

	public void setTempat_lahir(String tempat_lahir) {
		this.tempat_lahir = tempat_lahir;
	}

	public String getStatus_guru() {
		return status_guru;
	}

	public void setStatus_guru(String status_guru) {
		this.status_guru = status_guru;
	}

}
