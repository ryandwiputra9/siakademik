package com.sia.entity;

import java.util.Date;

public class ActivityLog {
	private String activityType;
	private String activityName;
	private Date DateAct;
	private int userId;
	private String moduleName;
	private String moduleCode;
	private String ipAddress;
	private String userAgent;
	
	private Users users;
	
	
	public Users getUser() {
		return users;
	}

	public void setUser(Users user) {
		this.users = user;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public Date getDateAct() {
		return DateAct;
	}

	public void setDateAct(Date dateAct) {
		DateAct = dateAct;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
}
