package com.sia.entity;

public class Walikelas extends Guru{
	private String id_walikelas;
	private String id_guru;
	private String tahun_ajaran;
	private String kelas;


	public String getId_walikelas() {
		return id_walikelas;
	}

	public void setId_walikelas(String id_walikelas) {
		this.id_walikelas = id_walikelas;
	}

	public String getId_guru() {
		return id_guru;
	}

	public void setId_guru(String id_guru) {
		this.id_guru = id_guru;
	}

	public String getTahun_ajaran() {
		return tahun_ajaran;
	}

	public void setTahun_ajaran(String tahun_ajaran) {
		this.tahun_ajaran = tahun_ajaran;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

}
