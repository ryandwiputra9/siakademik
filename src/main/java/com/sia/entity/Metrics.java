package com.sia.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Metrics {
	@JsonProperty("mem")
	String mem;
	@JsonProperty("mem.free")
	String mem_free;
	@JsonProperty("processors")
	String processors;
	@JsonProperty("uptime")
	String uptime;
	
	public String getFreeMemPercent() {
		int total = Integer.valueOf(mem);

		int free = Integer.valueOf(mem_free);
		int percent = (free * 100) / total;
		String toString = String.valueOf(percent);
		System.out.println("Free Ram E :  " + (free * 100) / total + " %");
		return toString;
	}

	public String getUsedMemPercent() {
		int total = Integer.valueOf(mem);
		int free = Integer.valueOf(mem_free);
		int used = total - free;
		int percent = (used * 100) / total;
		System.out.println("Used Ram E :  " + (used * 100) / total + " %");
		return String.valueOf(percent);
	}
	

	public String getMem() {
		return mem;
	}

	public void setMem(String mem) {
		this.mem = mem;
	}

	public String getMem_free() {
		return mem_free;
	}

	public void setMem_free(String mem_free) {
		this.mem_free = mem_free;
	}

	public String getProcessors() {
		return processors;
	}

	public void setProcessors(String processors) {
		this.processors = processors;
	}

	public String getUptime() {
		return uptime;
	}

	public void setUptime(String uptime) {
		this.uptime = uptime;
	}

	
}
