package com.sia.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.sia.entity.Email;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public class EmailService {

	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	private SpringTemplateEngine templateEngine;

	public void sendSimpleMessage(Email mail) throws MessagingException, IOException {
		MimeMessage message = emailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				StandardCharsets.UTF_8.name());

		helper.addAttachment("logo.png", new ClassPathResource("java-logo.jpg"));

		Context context = new Context();
		context.setVariables(mail.getModel());
		/*context.setVariable("name", "LINUX MINT");
		System.out.println("hashMap : "+mail.getModel());
		System.out.println("hashMap2 : "+context.toString());*/
		
		String html = templateEngine.process("mail/mail-template", context);
		System.out.println("Template ---- "+html);
		helper.setTo(mail.getTo());
		helper.setText(html, true);
		helper.setSubject(mail.getSubject());
		helper.setFrom(mail.getFrom());
		
		emailSender.send(message);
	}
	
	@Autowired
    private Configuration configFreemaker;

    public void sendSimpleMessageFreemaker(Email mail) throws MessagingException, IOException, TemplateException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,
                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        helper.addAttachment("logo.png", new ClassPathResource("templates/mail/java-logo.jpg"));
         configFreemaker = new Configuration();
         configFreemaker.setClassForTemplateLoading(this.getClass(), "/");
       //  Template freemarkerTemplate = freemarkerConfiguration.getTemplate("template.tpl");
        Template t = configFreemaker.getTemplate("templates/fremaker-mail-template.fml");
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.getModel());
        System.out.println("HTML : ------"+html);
        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());

      //  emailSender.send(message);
    }

}
