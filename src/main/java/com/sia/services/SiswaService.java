package com.sia.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sia.dao.siswaDao;
import com.sia.entity.Siswa;

@Service
public class SiswaService {
	@Autowired
	CustomUserDetailsService userDetail;
	
	@Autowired
	private siswaDao dao;
	  private static final Logger logger = LoggerFactory.getLogger(SiswaService.class);
	public void insertSiswa(Siswa s) {
		dao.insertSiswa(s);
	}

	public List<Siswa> getListSiswa() {
		
	//	logger.debug("Testing"+userDetail.getUser());
	//	logger.debug("USER ID : "+userDetail.getUser().getId());
	//	logger.debug("USER NAME : "+userDetail.getUser().getUsername());
		
		/* Dummy
		 * ArrayList<Siswa> temp=new ArrayList<Siswa>(); Siswa a=new Siswa();
		 * a.setId("1"); a.setNama("Ridho"); a.setNis("9999123"); a.setNisn("123331");
		 * a.setTanggalLahir("18 Oct 1994"); a.setAgama("ISLAM");
		 * a.setTempatLahir("Jakarta"); a.setTlp("0888877712"); temp.add(a); Siswa b=new
		 * Siswa(); b.setId("2"); b.setNama("Testinh"); b.setNis("9999123");
		 * b.setNisn("123331"); b.setTanggalLahir("12 Oct 1994"); b.setAgama("ISLAM");
		 * b.setTempatLahir("Jakarta"); b.setTlp("123444"); temp.add(b);
		 */

		List<Siswa> list = dao.findAllSiswa();

		return list;
	}

	public Siswa getSiswaById(Siswa s) {
		Siswa siswa = dao.findSiswaById(s.getId());
		return siswa;
	}

	public void deleteSiswa(Siswa s) {
		dao.deleteSiswa(s);

	}

	public List<Siswa> updateSiswa(Siswa s) {
		dao.updateSiswa(s);
		
		/* reload List all siswa*/
		List<Siswa> list=dao.findAllSiswa();
		return list;
	}

	public ArrayList<Siswa> findByNameOrId(Siswa s) {
		ArrayList<Siswa> list = dao.findByNameOrId(s);
		return list;
	}
}
