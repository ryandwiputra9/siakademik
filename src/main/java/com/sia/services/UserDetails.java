package com.sia.services;

import org.springframework.security.core.context.SecurityContextHolder;

import com.sia.entity.CustomUserDetails;

public class UserDetails {
	public CustomUserDetails getUser() {
		CustomUserDetails userDetail = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		return userDetail;
	}
}
