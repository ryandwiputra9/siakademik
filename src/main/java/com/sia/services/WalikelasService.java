package com.sia.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sia.dao.walikelasDao;
import com.sia.entity.Walikelas;

@Service
public class WalikelasService {
	@Autowired
	private walikelasDao dao;

	public void insertWalikelas(Walikelas s) {
		dao.insertWalikelas(s);
	}

	public List<Walikelas> getListWalikelas() {
		List<Walikelas> list = dao.findAllWalikelas();

		return list;
	}

	public Walikelas getWalikelasById(Walikelas s) {
		Walikelas Walikelas = dao.findWalikelasById(s);
		return Walikelas;
	}

	public void deleteWalikelas(Walikelas s) {
		dao.deleteWalikelas(s);

	}

	public List<Walikelas> updateWalikelas(Walikelas s) {
		dao.updateWalikelas(s);

		/* reload List all Walikelas */
		List<Walikelas> list = dao.findAllWalikelas();
		return list;
	}

	public ArrayList<Walikelas> getListByNameOrId(Walikelas s) {
		ArrayList<Walikelas> list = dao.getListByNameOrId(s);
		return list;
	}

	public ArrayList<Walikelas> getListWalikelasDummy() {
		Walikelas a = new Walikelas();
		a.setId_guru("02");
		a.setId_walikelas("12");

		a.setNama("Testing");
		a.setNip("099999913");
		a.setKelas("7.5");
		Walikelas b = new Walikelas();
		b.setId_guru("02");
		b.setId_walikelas("12");

		b.setNama("Testing");
		b.setNip("099999913");
		b.setKelas("7.5");

		ArrayList<Walikelas> list = new ArrayList<Walikelas>();
		list.add(a);
		list.add(b);

		return list;
	}

	public static void main(String[] args) {
		Walikelas e = new Walikelas();
		e.setNama("Zeros");
		e.setEmail("Zero@gmail.com");
		Walikelas a = new Walikelas();
		a.setNama("Ryan");
		a.setEmail("ryan@gmail.com");
		Walikelas b = new Walikelas();
		b.setNama("Dwi");
		b.setEmail("dwi@gmail.com");
		Walikelas c = new Walikelas();
		c.setNama("Putra");
		c.setEmail("putra@gmail.com");
		Walikelas d = new Walikelas();
		d.setNama("Testing");
		d.setEmail("testing@gmail.com");
		

		List<Walikelas> personList = new ArrayList<>();
		personList.add(a);
		personList.add(b);
		personList.add(c);
		personList.add(d);
		personList.add(e);
		
		/* Sorting with Colection */
		
		Sortbyname sorta = new Sortbyname();
		for (Walikelas ax : personList) {
			System.out.println("Before Sort With Collection : " + ax.getNama() + " : " + ax.getEmail());
		}
		System.out.println();
		System.out.println("-------Sort With Collections---------");
		Collections.sort(personList, sorta);
		for (Walikelas ax : personList) {
			System.out.println("After Sort With Collection : " + ax.getNama() + " : " + ax.getEmail());
		}
		System.out.println();
		System.out.println("-------Sort With Lamda java 8---------");
		/* sort lamda java 8 */
		
		//Descending
		personList.sort((Walikelas p1, Walikelas p2) -> p2.getNama().compareTo(p1.getNama()));
		//ascending
//		personList.sort((Walikelas p1, Walikelas p2) -> p1.getNama().compareTo(p2.getNama()));
		for (Walikelas ax : personList) {
			System.out.println("" + ax.getNama() + " " + ax.getEmail());
		}

		System.out.println();

		/*Sort with colection Array String*/
		System.out.println("-------Sort With ArrayList String ---------");
		ArrayList<String> al = new ArrayList<String>();
		al.add("Geeks For Geeks");
		al.add("Friends");
		al.add("Dear");
		al.add("Is");
		al.add("Superb");

		/*
		 * Collections.sort method is sorting the elements of ArrayList in ascending
		 * order.
		 */
		Collections.sort(al, Collections.reverseOrder());

		// Let us print the sorted list
		System.out.println("List after the use of" + " Collection.sort() :\n" + al);

	}

	 static class Sortbyname implements Comparator<Walikelas> {
		// Used for sorting in ascending order of
		// roll name
		@Override
		public int compare(Walikelas a, Walikelas b) {
			return a.getNama().compareTo(b.getNama());
		}
	}
}
