package com.sia.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sia.dao.activityDao;
import com.sia.dao.siswaDao;
import com.sia.entity.ActivityLog;
import com.sia.entity.Siswa;

@Service
public class ActivityService {
	@Autowired
	CustomUserDetailsService userDetail;

	@Autowired
	private activityDao dao;
	private static final Logger logger = LoggerFactory.getLogger(ActivityService.class);

	public List<ActivityLog> getAllActivityUser() {

		List<ActivityLog> list = dao.checkAlluserActivity();

		return list;
	}

	/*
	 * public ActivityLog getUserActivity(ActivityLog s) { ActivityLog act =
	 * dao.getUserActivity(s.getUserId()); return act; }
	 * 
	 * 
	 * 
	 * public ArrayList<Siswa> findByNameOrId(Siswa s) { ArrayList<Siswa> list =
	 * dao.findByNameOrId(s); return list; }
	 */
}
