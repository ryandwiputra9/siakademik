package com.sia.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sia.dao.userDao;
import com.sia.entity.CustomUserDetails;
import com.sia.entity.Users;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	public CustomUserDetails getUser() {
		CustomUserDetails userDetail = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		return userDetail;
	}

	@Autowired
	private userDao dao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Users> optionalUsers = dao.findByName(username);

		System.out.println("Optional :: " + optionalUsers.get());
		Users u = optionalUsers.get();

		// u.setRoles(roles);
		System.out.println("RoleName : " + u.getRole().getRole_name() + "  Role ID :  " + u.getRole().getRole_id());

		optionalUsers.orElseThrow(() -> new UsernameNotFoundException("Username not found"));
		return optionalUsers.map(CustomUserDetails::new).get();
	}
	
	/*public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetail user = dao.findByLoginName(username);
		if (user == null) {
			throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
		} else {
			// user is found; map its roles
			Collection<GroupDetail> roles = groupMapper.findGroupDetailsById(String.valueOf(user.getIdMap()));
			user.setGroupDetailList(roles);
			if (!user.hasRole(UserDetailsHelper.ROLE_USER))
				user.addRole(UserDetailsHelper.ROLE_USER);
			if (!user.hasRole(UserDetailsHelper.ROLE_ANONYMOUS))
				user.addRole(UserDetailsHelper.ROLE_ANONYMOUS);
			logger.debug("Found user: {}",user);
			return user;
		}
	}*/
}
