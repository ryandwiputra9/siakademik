package com.sia.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.sia.entity.Siswa;
import com.sia.services.SiswaService;

@Controller
public class siswaController {
	@Autowired
	private SiswaService service;

	@GetMapping("/siswa")
	public String getAllSiswa(Model model) {
		List<Siswa> s = service.getListSiswa();
		model.addAttribute("listSiswa", s);
		return "siswa/listsiswa";
	}

	@GetMapping("/add")
	public String FormSiswa(Model model) {
		Siswa a = new Siswa();
		model.addAttribute("Siswa", a);
		return "formSiswa";
	}
	
	public static void main(String[] args) {
		 HashMap<String, Object> capitalCities = new HashMap<String, Object>();
			ArrayList<Siswa> temp=new ArrayList<Siswa>();
		 Siswa a=new Siswa();
		 /*
			a.setId("1");
			a.setNama("Ridho");
			a.setNis("9999123");
			a.setNisn("123331");
			a.setTanggalLahir("18 Oct 1994");
		//	a.setAgama("ISLAM");
			a.setTempatLahir("Jakarta");
			a.setTlp("0888877712");
			temp.add(a);
			Siswa b=new Siswa();
			b.setId("2");
			b.setNama("Testinh");
			b.setNis("9999123");
			b.setNisn("123331");
			b.setTanggalLahir("12 Oct 1994");
		//	b.setAgama("ISLAM");
			b.setTempatLahir("Jakarta");
			b.setTlp("123444");
			
		 	
		    // Add keys and values (Country, City)
		    capitalCities.put("Siswa",a);
		    capitalCities.put("List",temp);

		    System.out.println(capitalCities); 
		    
		    System.out.println(" KEY LIST "+capitalCities.get("List"));
		    for (Object i : capitalCities.values()) {
		    	  System.out.println(i);
		    	}
		    for (Object i : capitalCities.keySet()) {
		    	  System.out.println(i);
		    	}
		    	*/
	}
}
