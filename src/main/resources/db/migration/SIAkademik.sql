-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - Source distribution
-- Server OS:                    Linux
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for SIAkademik
CREATE DATABASE IF NOT EXISTS `SIAkademik` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `SIAkademik`;

-- Dumping structure for table SIAkademik.GURU
CREATE TABLE IF NOT EXISTS `GURU` (
  `ID_GURU` varchar(50) DEFAULT NULL,
  `NIP` varchar(50) DEFAULT NULL,
  `NAMA` varchar(120) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(50) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `LULUSAN` varchar(50) DEFAULT NULL,
  `TELEPHONE` text,
  `EMAIL` text,
  `JABATAN` varchar(50) DEFAULT NULL,
  `STATUS_GURU` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.GURU: ~1 rows (approximately)
/*!40000 ALTER TABLE `GURU` DISABLE KEYS */;
INSERT INTO `GURU` (`ID_GURU`, `NIP`, `NAMA`, `TEMPAT_LAHIR`, `TANGGAL_LAHIR`, `AGAMA`, `LULUSAN`, `TELEPHONE`, `EMAIL`, `JABATAN`, `STATUS_GURU`) VALUES
	('1', '201993', 'Ryan D Putra', 'Bekasi', '1994-02-21', 'ISLAM', 'S1', '08822444', 'ryan@gmail.com', 'GURU', 1);
/*!40000 ALTER TABLE `GURU` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.SISWA
CREATE TABLE IF NOT EXISTS `SISWA` (
  `ID` varchar(50) DEFAULT NULL,
  `NIS` int(11) DEFAULT NULL,
  `NISN` int(11) DEFAULT NULL,
  `NAMA` varchar(50) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(50) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `AGAMA` varchar(20) DEFAULT NULL,
  `TELEPHONE` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.SISWA: ~9 rows (approximately)
/*!40000 ALTER TABLE `SISWA` DISABLE KEYS */;
INSERT INTO `SISWA` (`ID`, `NIS`, `NISN`, `NAMA`, `TEMPAT_LAHIR`, `TANGGAL_LAHIR`, `AGAMA`, `TELEPHONE`) VALUES
	('1', 9988881, 123331, 'Linux mint 19 Sylvia', 'Afrika', '1990-10-05', 'ISLAM', '12345'),
	('2', 1224, 1235, 'Nikumi', 'Jakarta', '2000-02-18', 'ISLAM', '098887'),
	('3', 9991, 123331, 'Linux23', 'Jakarta', '1994-10-04', 'ISLAM', '0888877712'),
	('5', 9992, 123331, 'Mint', 'Jakarta', '1994-10-05', 'ISLAM', '0888877712'),
	('22', 9988881, 123331, 'Linux mint 19 Sylvia', 'Afrika', '1990-10-05', NULL, '12345'),
	('', 9988881, 123331, 'Linux mint 19 Sylvia', 'Afrika', '1990-10-05', NULL, '12345'),
	(NULL, 9988881, 123331, 'Linux mint 19 Sylvia', 'Afrika', '1990-10-05', NULL, '12345'),
	(NULL, 9988881, 123331, 'Linux mint 19 Sylvia', 'Afrika', '1990-10-05', NULL, '12345'),
	(NULL, 9988881, 123331, 'Linux mint 19 Sylvia', 'Afrika', '1990-10-05', NULL, '12345'),
	('', 9988881, 123331, 'Linux mint 19 Sylvia', 'Afrika', '1990-10-05', NULL, '12345');
/*!40000 ALTER TABLE `SISWA` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.USERS
CREATE TABLE IF NOT EXISTS `USERS` (
  `ID` varchar(50) DEFAULT NULL,
  `USERNAME` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(256) DEFAULT NULL,
  `ENABLED` varchar(20) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `ROLE` int(11) DEFAULT NULL,
  `NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.USERS: ~0 rows (approximately)
/*!40000 ALTER TABLE `USERS` DISABLE KEYS */;
INSERT INTO `USERS` (`ID`, `USERNAME`, `PASSWORD`, `ENABLED`, `STATUS`, `ROLE`, `NAME`) VALUES
	('1', 'linux@gmail.com', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', '1', 1, 0, 'sia');
/*!40000 ALTER TABLE `USERS` ENABLE KEYS */;

-- Dumping structure for table SIAkademik.WALI_KELAS
CREATE TABLE IF NOT EXISTS `WALI_KELAS` (
  `ID_WALIKELAS` varchar(50) DEFAULT NULL,
  `ID_GURU` varchar(50) DEFAULT NULL,
  `KELAS` varchar(50) DEFAULT NULL,
  `TAHUN_AJARAN` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table SIAkademik.WALI_KELAS: ~1 rows (approximately)
/*!40000 ALTER TABLE `WALI_KELAS` DISABLE KEYS */;
INSERT INTO `WALI_KELAS` (`ID_WALIKELAS`, `ID_GURU`, `KELAS`, `TAHUN_AJARAN`) VALUES
	('1', '1', '92', '2019-2020');
/*!40000 ALTER TABLE `WALI_KELAS` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
